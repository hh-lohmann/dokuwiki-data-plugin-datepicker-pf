dataPluginDatepickerPF Plugin for DokuWiki

PRIMITIVE ADJUSTING OF DATA EDITOR'S US ENGLISH SETTINGS FOR DATEPICKER

The JavaScript way was chosen due to more flexibility over trying to change the
data plugin itself.

All documentation for this plugin can be found at
https://bitbucket.org/hh-lohmann/dokuwiki-data-plugin-datepicker

If you install this plugin manually, make sure it is installed in
lib/plugins/dataPluginDatepickerPF/ - if the folder is called different it
will not work!

Please refer to http://www.dokuwiki.org/plugins for additional info
on how to install plugins in DokuWiki.

----
Copyright (C) hh.lohmann <hh.lohmann@yahoo.de>

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; version 2 of the License

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

See the COPYING file in your DokuWiki folder for details
